import numpy as np
import pandas as pd
import sklearn.tree as tree
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold
from sklearn.metrics import confusion_matrix


import pandas as pd
import numpy as np
from sklearn.metrics import f1_score,confusion_matrix, classification_report,accuracy_score,precision_score,recall_score,roc_auc_score
import os
import matplotlib.pyplot as plt
# import seaborn as sns
from scipy.stats import spearmanr
from scipy.cluster import hierarchy
from collections import defaultdict
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import KFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier

from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from sklearn import metrics
from sklearn.metrics import roc_curve, auc, roc_auc_score
#
# df=pd.read_csv(r"C:\Users\24539\PycharmProjects\pgh\data\Data.csv")
# df1=df.loc[df.columns[0]=='2013B']
# print(df1)

# index1=['t'+str(i) for i in range(3)]
# index2=['click'+str(i) for i in range(800)]
# index3=['demo'+str(i) for i in range(36)]
# index=index1+index2+index3
df=pd.read_csv(r'C:\Users\24539\Desktop\论文\dataindex.csv')

# df=df.loc[df['lesson']=='DDD']
X = df.iloc[:,3:839]
y = df.iloc[:,839:840]

parameters = {'min_impurity_decrease': [0.05 * i for i in range(3)],
              'criterion': ["gini", "entropy"]}

dtc = tree.DecisionTreeClassifier()  # Setup 10-fold CV
kf = KFold(n_splits=10, shuffle=True)
matrix = np.matrix('0 0; 0 0')  # Generating confusion matrix

for train_index, test_index in kf.split(X):  # Iterating through each fold of CV
    X_train, X_test = X.iloc[train_index], X.iloc[test_index]
    y_train, y_test = y.iloc[train_index], y.iloc[test_index]
    gs_dtc = GridSearchCV(dtc, parameters, scoring="accuracy", cv=10)
    gs_dtc.fit(X_train, y_train)
    best_index = gs_dtc.cv_results_['rank_test_score'].argmin()
    best_param = gs_dtc.cv_results_['params'][
        best_index]  # Perform GridSearchCV using training data and get the best parameter
    dtc_cv = tree.DecisionTreeClassifier(**best_param)
    dtc_cv.fit(X_train, y_train)
    y_pred = dtc_cv.predict(X_test)
    matrix = np.add(matrix, confusion_matrix(y_test, y_pred))  # Build model and evaluate with testing data
    f1 = f1_score(y_true=y_test, y_pred=y_pred, average='weighted')
    accuracy = accuracy_score(y_true=y_test, y_pred=y_pred)
    precision = precision_score(y_true=y_test, y_pred=y_pred)
    recall = recall_score(y_true=y_test, y_pred=y_pred)
    auc = roc_auc_score(y_test, y_pred)
print(f1,accuracy,precision,recall,auc)